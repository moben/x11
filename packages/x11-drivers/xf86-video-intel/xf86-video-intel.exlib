# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Copyright 2008, 2009, 2010, 2016 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam libdrm

require xorg

if ever is_scm || ever at_least 2.99.917_p20170313; then
    require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
fi

export_exlib_phases src_prepare src_configure src_install

SUMMARY="Xorg video driver for Intel-based cards"

BUGS_TO+=" compnerd@compnerd.org"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="debug ( providers: eudev systemd ) [[ number-selected = exactly-one ]]"

if ! ever at_least 2.99.917; then
    if ever at_least 2.20; then
        MYOPTIONS+="
            sna [[ description = [ Use only SNA - the new acceleration backend ] ]]
        "
    else
        MYOPTIONS+="
            sna [[ description = [ Enable SandyBridge's New Acceleration ] ]]
        "
    fi
else
    MYOPTIONS+="
        tools  [[ description = [ Build intel-backlight-helper and intel-virtual-output tool ] ]]
    "
fi

DEPENDENCIES="
    build:
        x11-proto/xorgproto
    build+run:
        x11-dri/libdrm[>=$(exparam libdrm)][video_drivers:intel(+)]
        x11-dri/mesa[>=7.3_rc2][video_drivers:intel(+)]
        x11-libs/libX11[xcb(+)]
        x11-libs/libXext
        x11-libs/libXrender
        x11-libs/libXvMC
        x11-libs/libpciaccess[>=0.10]
        x11-libs/libxcb
        x11-libs/pixman:1[>=0.24] [[ note = [ we have no older so depend on the newest (required by 2.18) ] ]]
        x11-server/xorg-server[>=1.6][dri2(+)]
        x11-utils/xcb-util [[ note = [ works with 0.3.8, too ] ]]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd ) [[ note = [ Automagic dep, enables udev-based monitor hotplug detection ] ]]
"

if ! ever at_least 2.21.15-r1; then
    DEPENDENCIES+="
        build+run:
            x11-server/xorg-server[<1.15]
    "
fi

if ever at_least 2.21.11; then
    DEPENDENCIES+="
        build+run:
            x11-libs/pixman:1[>=0.27.1]
    "
    if ever at_least 2.99.917; then
        DEPENDENCIES+="
            build+run:
                media-libs/libpng:= [[ note = [ needed for tests ] ]]
                x11-libs/cairo[X]   [[ note = [ needed for tests ] ]]
                x11-libs/libXrandr
                x11-libs/libXv
                x11-libs/libXxf86vm
                x11-libs/libxshmfence
                tools? (
                    x11-libs/libXcursor
                    x11-libs/libXdamage
                    x11-libs/libXfixes
                    x11-libs/libXinerama
                    x11-libs/libXScrnSaver
                    x11-libs/libXtst
                ) [[ note = [ Automagic deps for intel-virtual-output ] ]]
       "
    fi
else
    DEPENDENCIES+="
        build+run:
            x11-libs/libXfixes
            sna? ( x11-server/xorg-server[>=1.10] )
    "
fi

DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-xvmc )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( debug )

if ever at_least 2.20.7; then
    # Disable deprecated stuff, uxa should work with everything
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --disable-dga
        --disable-xaa
        --enable-udev
    )
    ! ever at_least 2.99.917 && DEFAULT_SRC_CONFIGURE_OPTION_ENABLES+=( '!sna uxa' )
else
    DEFAULT_SRC_CONFIGURE_PARAMS+=( --enable-vmap )
fi

if ! ever at_least 2.99.917; then
    DEFAULT_SRC_CONFIGURE_OPTION_ENABLES+=( sna )
    DEFAULT_SRC_CONFIGURE_PARAMS+=( --enable-dri )
else
    DEFAULT_SRC_CONFIGURE_OPTION_ENABLES+=(
        tools
        'tools backlight-helper'
    )
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --enable-backlight
        --enable-dri{,1,2,3}
        --enable-kms
        --enable-sna
        --enable-ums
        --enable-uxa
    )
fi

if ever at_least 2.99.917_p20160224; then
    DEFAULT_SRC_CONFIGURE_PARAMS+=( --with-default-dri=3 )
fi

xf86-video-intel_src_prepare() {
    # fix socket path for bumblebee.socket
    if ever at_least 2.99.917; then
        edo sed \
            -e 's:/var/run:/run:g' \
            -i tools/virtual.c
    fi

    if ever is_scm || ever at_least 2.99.917_p20170313; then
        autotools_src_prepare
    else
        default
    fi
}

xf86-video-intel_src_configure() {
    local myconf=(
        "${DEFAULT_SRC_CONFIGURE_PARAMS[@]}" \
        $(for s in "${DEFAULT_SRC_CONFIGURE_OPTION_ENABLES[@]}" ; do \
            option_enable ${s} ; \
        done )
    )

    if ever at_least 2.99.917; then
        myconf+=(
            # WriteCombining mmaps is considered experimental
            --disable-wc-mmap
            --with-default-accel=sna
        )
    fi

    econf ${myconf[@]}
}

xf86-video-intel_src_install() {
    default

    if ever at_least 2.99.917; then
        option tools || edo rm -rf "${IMAGE}"/usr/bin
    fi
}

